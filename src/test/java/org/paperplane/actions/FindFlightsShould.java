package org.paperplane.actions;

import org.junit.Before;
import org.junit.Test;
import org.paperplane.DefaultValues;
import org.paperplane.domain.SearchDto;
import org.paperplane.domain.Trip;
import org.paperplane.domain.TripDto;
import org.paperplane.repository.TripRepository;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Double.parseDouble;
import static java.util.Arrays.asList;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FindFlightsShould {

    private FindFlights findFlights;
    private TripRepository tripRepository;

    private static final int TWO_PASSENGERS = 2;
    private static final String ORIGIN = "LPA";
    private static final String DESTINATION = "LGW";

    @Before
    public void setUp() throws Exception {
        tripRepository = mock(TripRepository.class);
        findFlights = new FindFlights(tripRepository);
    }

    @Test
    public void retrieve_a_price_of_flights_multiplied_by_the_number_of_passengers() {
        final int numberOfChildren = 0;
        final Trip flight = anyFlightWith("120.0");
        SearchDto tripDto = anySearchDtoWith(TWO_PASSENGERS, numberOfChildren);
        when(tripRepository.getTrips(tripDto)).thenReturn(asList(flight));

        final List<TripDto> flights = findFlights.execute(tripDto);

        TripDto expectedFlight = expectedFlightWith("240.0", TWO_PASSENGERS, numberOfChildren);
        assertThat(flights).containsOnlyOnce(expectedFlight);
    }

    private SearchDto anySearchDtoWith(int numberOfPassengers, int numberOfChildren) {
        final SearchDto tripDto = new SearchDto();
        tripDto.origin = ORIGIN;
        tripDto.destination = DESTINATION;
        tripDto.numberOfPassengers = numberOfPassengers;
        tripDto.numberOfChildren = numberOfChildren;
        return tripDto;
    }

    @Test
    public void retrieve_a_price_of_flights_multiplied_by_the_number_of_passengers_and_added_the_price_of_children() {
        final int numberOfChildren = 1;
        final Trip flight = anyFlightWith("120.0");
        SearchDto tripDto = anySearchDtoWith(TWO_PASSENGERS, numberOfChildren);
        when(tripRepository.getTrips(tripDto)).thenReturn(asList(flight));

        final List<TripDto> flights = findFlights.execute(tripDto);

        TripDto expectedFlight = expectedFlightWith("300.0", TWO_PASSENGERS, numberOfChildren);
        assertThat(flights).containsOnlyOnce(expectedFlight);
    }

    private Trip anyFlightWith(String price) {
        return new Trip(new ArrayList<>());
                /*.origin(ORIGIN)
                .destination(DESTINATION)
                .departureTime(DefaultValues.todayDate)
                .arrivalTime(DefaultValues.tomorrowDate)
                .price(parseDouble(price));*/
    }

    private TripDto expectedFlightWith(String price, int numberOfPassengers, int numberOfChildren) {
        final TripDto tripDto = new TripDto();
        tripDto.origin = ORIGIN;
        tripDto.destination = DESTINATION;
        tripDto.price = parseDouble(price);
        tripDto.numberOfPassengers = numberOfPassengers;
        tripDto.numberOfChildren = numberOfChildren;
        tripDto.departureDateTime = DefaultValues.todayTimeString;
        tripDto.arrivalDateTime = DefaultValues.tomorrowTimeString;
        return tripDto;
    }
}
