package org.paperplane;

import java.time.LocalDateTime;

public class DefaultValues {
    public static String todayTimeString = "2017-12-01 12:00";
    public static String tomorrowTimeString = "2017-12-02 12:00";
    public static LocalDateTime todayDate = PaperplaneDateFormatter.parseFromString(todayTimeString);
    public static LocalDateTime tomorrowDate = PaperplaneDateFormatter.parseFromString(tomorrowTimeString);
}
