package org.paperplane.repository;

import org.junit.Before;
import org.junit.Test;
import org.paperplane.domain.SearchDto;
import org.paperplane.domain.Trip;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

public class TripRepositoryShould extends BaseRepositoryShould {

    @Override
    protected List<String> deleteFromTables() {
        return asList("Trips");
    }

    private Sql2o connection;
    private TripRepository tripRepository;

    @Before
    public void given_a_repository_and_a_database() {
        connection = new Sql2o(connectionTestDatabase, dbUser, dbPassword);
        tripRepository = new TripRepository(connectionTestDatabase);
    }

    @Test
    public void get_a_single_trip() {
        Trip flight =
                new Trip(new ArrayList<>())
                        /*.price(parseDouble("120.0"))
                        .origin("ORIGIN")
                        .destination("LGW")
                        .departureTime(parseFromString("2017-12-02 11:00"))
                        .arrivalTime(parseFromString("2017-12-02 15:00"))*/;
        insertA(flight, "flight");

        Trip trip = tripRepository.getTrips(getSearchDtoFrom(flight)).get(0);
        /*assertThat(trip.origin()).isEqualTo(flight.origin());
        assertThat(trip.destination()).isEqualTo(flight.destination());
        assertThat(trip.departureTime()).isEqualTo(flight.departureTime());
        assertThat(trip.arrivalTime()).isEqualTo(flight.arrivalTime());*/
        assertThat(trip.price()).isEqualTo(flight.price());
    }

    private SearchDto getSearchDtoFrom(Trip trip) {
        final SearchDto searchDto = new SearchDto();
        /*searchDto.origin = trip.origin();
        searchDto.destination = trip.destination();*/
        return searchDto;
    }

    private void insertA(Trip trip, String tripType) {
        try (Connection connection = this.connection.open()) {
            connection.createQuery(
                    "INSERT INTO trips(TRIP_TYPE, ORIGIN, DESTINATION, DEPARTURE_DATE, DEPARTURE_TIME, ARRIVAL_DATE, ARRIVAL_TIME, PRICE)" +
                            "VALUES (:tripType, :origin, :destination, :departureDate, :departureTime, :arrivalDate, :arrivalTime, :price)")
                    .addParameter("tripType", tripType)
                    /*.addParameter("origin", trip.origin())
                    .addParameter("destination", trip.destination())*
                    .addParameter("departureDate", parseDateToString(trip.departureTime()))
                    .addParameter("departureTime", "11:00")
                    .addParameter("arrivalDate", parseDateToString(trip.arrivalTime()))*/
                    .addParameter("arrivalTime", "15:00")
                    .addParameter("price", String.valueOf(trip.price()))
                    .executeUpdate();
        }
    }
}
