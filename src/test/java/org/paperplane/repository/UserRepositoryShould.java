package org.paperplane.repository;

import org.junit.Before;
import org.junit.Test;
import org.paperplane.domain.RegisteredUser;
import org.paperplane.domain.UserDto;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

public class UserRepositoryShould extends BaseRepositoryShould {
    @Override
    protected List<String> deleteFromTables() {
        return asList("Users");
    }

    private Sql2o connection;
    private UserRepository userRepository;

    @Before
    public void given_a_repository_and_a_database() {
        connection = new Sql2o(connectionTestDatabase, dbUser, dbPassword);
        userRepository = new UserRepository(connectionTestDatabase);
    }

    @Test
    public void get_a_user_by_username_and_password() {
        String username = "anyUsername";
        String password = "anyPassword";
        UserDto userDto = new UserDto();
        userDto.username = username;
        userDto.password = password;
        insertA(userDto);

        RegisteredUser user = userRepository.get(userDto);

        assertThat(user.getUsername()).isEqualTo(username);
        assertThat(user.getPassword()).isEqualTo(password);
    }

    private void insertA(UserDto userDto) {
        try (Connection connection = this.connection.open()) {
            connection.createQuery(
        "INSERT INTO USERS(name, surname, email, username, password)" +
                "VALUES (:name, :surname, :email, :username, :password)")
                    .addParameter("username", userDto.username)
                    .addParameter("name", "anyName")
                    .addParameter("surname", "anySurname")
                    .addParameter("password", userDto.password)
                    .addParameter("email", "anyEmail")
                    .executeUpdate();
        }
    }
}
