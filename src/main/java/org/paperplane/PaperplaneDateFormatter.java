package org.paperplane;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class PaperplaneDateFormatter {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static LocalDateTime parseFromString(String date) {
        return LocalDateTime.parse(date, dateTimeFormatter);
    }

    public static String parseDateToString(LocalDateTime date) {
        return dateFormatter.format(date);
    }

    public static String parseDateTimeToString(LocalDateTime date) {
        return dateTimeFormatter.format(date);
    }
}
