package org.paperplane.domain;

public class Company {
    private final String name;
    private final String logo;

    public Company(String name, String logo) {
        this.name = name;
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public String getLogo() {
        return logo;
    }
}
