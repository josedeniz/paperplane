package org.paperplane.domain;

public class SearchDto {
    public String origin;
    public String tripType;
    public boolean isSingle;
    public String returnDate;
    public String destination;
    public String departureDate;
    public int numberOfChildren;
    public int numberOfPassengers;
}
