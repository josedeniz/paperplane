package org.paperplane.domain;

import java.util.List;

public class Journey {

    private final List<Stretch> stretches;

    public Journey(List<Stretch> stretches) {
        this.stretches = stretches;
    }

    public List<Stretch> getStretches() {
        return stretches;
    }
}
