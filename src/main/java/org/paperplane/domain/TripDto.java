package org.paperplane.domain;

public class TripDto {
    public String origin;
    public double price;
    public String destination;
    public int numberOfPassengers;
    public int numberOfChildren;
    public String departureDateTime;
    public String arrivalDateTime;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TripDto tripDto = (TripDto) o;

        if (Double.compare(tripDto.price, price) != 0) return false;
        if (numberOfPassengers != tripDto.numberOfPassengers) return false;
        if (numberOfChildren != tripDto.numberOfChildren) return false;
        if (origin != null ? !origin.equals(tripDto.origin) : tripDto.origin != null) return false;
        if (destination != null ? !destination.equals(tripDto.destination) : tripDto.destination != null) return false;
        if (departureDateTime != null ? !departureDateTime.equals(tripDto.departureDateTime) : tripDto.departureDateTime != null)
            return false;
        return arrivalDateTime != null ? arrivalDateTime.equals(tripDto.arrivalDateTime) : tripDto.arrivalDateTime == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = origin != null ? origin.hashCode() : 0;
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (destination != null ? destination.hashCode() : 0);
        result = 31 * result + numberOfPassengers;
        result = 31 * result + numberOfChildren;
        result = 31 * result + (departureDateTime != null ? departureDateTime.hashCode() : 0);
        result = 31 * result + (arrivalDateTime != null ? arrivalDateTime.hashCode() : 0);
        return result;
    }
}
