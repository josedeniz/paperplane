package org.paperplane.domain;

public class UserDto {
    public int id;
    public String username;
    public String password;
    public String name;
    public String surname;
    public String email;
}
