package org.paperplane.domain;

import java.time.LocalDateTime;

public class Stretch {
    private String from;
    private String to;
    private Company company;
    private LocalDateTime departureDateTime;
    private LocalDateTime arrivalDateTime;

    public String getFrom() {
        return from;
    }

    public Stretch setFrom(String from) {
        this.from = from;
        return this;
    }

    public String getTo() {
        return to;
    }

    public Stretch setTo(String to) {
        this.to = to;
        return this;
    }

    public Company getCompany() {
        return company;
    }

    public Stretch setCompany(Company company) {
        this.company = company;
        return this;
    }

    public LocalDateTime getDepartureDateTime() {
        return departureDateTime;
    }

    public Stretch setDepartureDateTime(LocalDateTime departureDateTime) {
        this.departureDateTime = departureDateTime;
        return this;
    }

    public LocalDateTime getArrivalDateTime() {
        return arrivalDateTime;
    }

    public Stretch setArrivalDateTime(LocalDateTime arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
        return this;
    }
}
