package org.paperplane.domain;

public interface NotificationSender {
    void send(RegisteredUser user, Message message);
}
