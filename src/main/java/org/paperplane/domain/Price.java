package org.paperplane.domain;

public class Price {
    private double amount;
    private Currency currency;

    public double getAmount() {
        return amount;
    }

    public Price setAmount(double amount) {
        this.amount = amount;
        return this;
    }

    public Currency getCurrency() {
        return currency;
    }

    public Price setCurrency(Currency currency) {
        this.currency = currency;
        return this;
    }
}
