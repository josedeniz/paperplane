package org.paperplane.domain;

public class RegisteredUser {

    private int id;
    private String name;
    private String surname;
    private String username;
    private String password;
    private String email;

    public RegisteredUser() {
        this.id = 0;
        this.name = "";
        this.surname = "";
        this.username = "";
        this.email = "";
        this.password = "";
    }

    public RegisteredUser(int id, String name, String surname, String username, String password, String email) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }
}
