package org.paperplane.domain;

public class ChoiceDto {
    public String id;
    public String origin;
    public String destination;
    public String companyName;
    public String departureDate;
    public String arrivalDate;
    public double price;
    public int scales;
    public String tripType;
}
