package org.paperplane.domain;

import java.util.List;

public class Trip {
    private Price price;
    private String tripType;
    private boolean isSingle;
    private int numberOfChildren;
    private int numberOfPassengers;
    private List<Journey> journeys;

    public Trip(List<Journey> journeys) {
        this.journeys = journeys;
    }

    public double price() {
        return price.getAmount();
    }

    public Trip price(double price) {
        this.price = new Price().setAmount(price);
        return this;
    }

    public String tripType() {
        return tripType;
    }

    public Trip tripType(String tripType) {
        this.tripType = tripType;
        return this;
    }

    public int numberOfChildren() {
        return numberOfChildren;
    }

    public Trip numberOfChildren(int numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
        return this;
    }

    public int numberOfPassengers() {
        return numberOfPassengers;
    }

    public Trip numberOfPassengers(int numberOfPassengers) {
        this.numberOfPassengers = numberOfPassengers;
        return this;
    }

    public List<Journey> journeys() {
        return journeys;
    }

    public boolean isSingle() {
        return isSingle;
    }

    public Trip isSingle(boolean single) {
        isSingle = single;
        return this;
    }
}
