package org.paperplane.infraestructure.config;

import org.paperplane.repository.SearchRepository;
import org.paperplane.repository.TripRepository;
import org.paperplane.repository.UserRepository;
import org.paperplane.repository.UserTripsRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class PersistenceConfiguration {

    @Value("${spring.datasource.url}")
    private String connectionUrl;

    @Bean
    @Primary
    public TripRepository postgreInvoiceRepository() {
        return new TripRepository(connectionUrl);
    }

    @Bean
    @Primary
    public UserRepository postgreUsersRepository() {
        return new UserRepository(connectionUrl);
    }

    @Bean
    @Primary
    public SearchRepository postgreSearchRepository() {
        return new SearchRepository(connectionUrl);
    }

    @Bean
    @Primary
    public UserTripsRepository postgreUserTripsRepository() {
        return new UserTripsRepository(connectionUrl);
    }

}
