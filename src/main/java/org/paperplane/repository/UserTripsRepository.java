package org.paperplane.repository;

import org.paperplane.domain.ChoiceDto;
import org.paperplane.domain.UserTrip;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import java.util.List;
import java.util.Map;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import static java.lang.String.valueOf;
import static java.util.stream.Collectors.toList;

public class UserTripsRepository {

    private final Sql2o sql2o;

    public UserTripsRepository(String connectionUrl) {
        this.sql2o = sql2o(connectionUrl);
    }

    private Sql2o sql2o(String connectionUrl) {
        return new Sql2o(connectionUrl, "paperplane", "12345");
    }

    public List<ChoiceDto> getFor(String userId) {
        String query = "SELECT T.Id, trip_type, \"FROM\", \"TO\", PRICE, DEPARTURE_DATE, DEPARTURE_TIME, ARRIVAL_DATE_TIME, NAME As Company_Name" +
                " FROM user_trips UT" +
                " INNER JOIN Stretch T ON T.ID = UT.trip_id" +
                " INNER JOIN COMPANY AS C on C.ID = COMPANY_ID" +
                " WHERE UT.user_id = :userId";

        List<Map<String, Object>> queryResults;
        try(Connection connection = sql2o.open()) {
            queryResults = connection.createQuery(query)
                    .addParameter("userId", parseInt(userId))
                    .executeAndFetchTable()
                    .asList();
        }

        return queryResults.stream()
                .map(this::toChoiceDto)
                .collect(toList());
    }

    public void save(UserTrip userTrip) {
        if (getFor(userTrip.userId).stream().map(c -> c.id).anyMatch(id -> userTrip.tripId.equals(id))) return;
        String query = "INSERT INTO USER_TRIPS (user_id, trip_id) " +
                       "VALUES (:userId, :tripId)";

        try(Connection connection = sql2o.open()) {
            connection.createQuery(query)
                    .addParameter("userId", parseInt(userTrip.userId))
                    .addParameter("tripId", parseInt(userTrip.tripId))
                    .executeUpdate();
        }
    }

    private ChoiceDto toChoiceDto(Map<String, Object> result) {
        ChoiceDto choiceDto = new ChoiceDto();
        choiceDto.id = valueOf(((int) result.get("id")));
        choiceDto.tripType = ((String) result.get("trip_type"));
        choiceDto.origin = ((String) result.get("from"));
        choiceDto.price = parseDouble(((String) result.get("price")));
        choiceDto.destination = ((String) result.get("to"));
        choiceDto.companyName = ((String) result.get("company_name"));
        choiceDto.departureDate = result.get("departure_date") + " " + result.get("departure_time");
        choiceDto.arrivalDate = ((String) result.get("arrival_date_time"));
        return choiceDto;
    }
}
