package org.paperplane.repository;

import org.paperplane.domain.SearchDto;
import org.paperplane.domain.Trip;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;
import static org.paperplane.PaperplaneDateFormatter.parseFromString;

public class TripRepository {
    private final Sql2o sql2o;

    public TripRepository(String connectionUrl) {
        this.sql2o = sql2o(connectionUrl);
    }

    private Sql2o sql2o(String connectionUrl) {
        return new Sql2o(connectionUrl, "paperplane", "12345");
    }

    public List<Trip> getTrips(SearchDto searchDto) {

        String query = "SELECT trips.origin, trips.destination, trips.price, trips.trip_type, trips.departure_date, trips.departure_time, trips.arrival_date, trips.arrival_time " +
                "FROM trips " +
                "WHERE origin = :origin " +
                "AND   trip_type = :tripType " +
                "AND   destination = :destination";

        List<Map<String, Object>> queryResults;
        try(Connection connection = sql2o.open()) {
            queryResults = connection.createQuery(query)
                    .addParameter("origin", searchDto.origin)
                    .addParameter("destination", searchDto.destination)
                    .addParameter("tripType", searchDto.tripType)
                    .executeAndFetchTable()
                    .asList();
        }

        return queryResults.stream()
                .map(this::toTrip)
                .collect(toList());
    }

    private Trip toTrip(Map<String, Object> result) {
        return getTrip(result);
                /*.origin((String) result.get("origin"))
                .destination((String) result.get("destination"))
                .price(parseDouble((String) result.get("price")))
                .departureTime(parseDate(result.get("departure_date") + " " + result.get("departure_time")))
                .arrivalTime(parseDate(result.get("arrival_date") + " " + result.get("arrival_time")));*/
    }

    private Trip getTrip(Map<String, Object> result) {
        return new Trip(new ArrayList<>());
    }

    private LocalDateTime parseDate(String date) {
        return parseFromString(date);
    }
}
