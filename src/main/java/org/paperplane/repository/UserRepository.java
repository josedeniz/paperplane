package org.paperplane.repository;

import org.paperplane.domain.RegisteredUser;
import org.paperplane.domain.UserDto;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

public class UserRepository {

    private final Sql2o sql2o;

    public UserRepository(String connectionUrl) {
        this.sql2o = sql2o(connectionUrl);
    }

    private Sql2o sql2o(String connectionUrl) {
        return new Sql2o(connectionUrl, "paperplane", "12345");
    }

    public RegisteredUser get(UserDto userDto) {
        String query = "SELECT id, name, username, surname, password, email" +
                " FROM users " +
                "WHERE username = :username " +
                "AND password = :password ";

        List<Map<String, Object>> queryResults;
        try(Connection connection = sql2o.open()) {
            queryResults = connection.createQuery(query)
                    .addParameter("username", userDto.username)
                    .addParameter("password", userDto.password)
                    .executeAndFetchTable()
                    .asList();
        }

        final List<RegisteredUser> users = queryResults.stream()
                .map(this::toRegisteredUser)
                .collect(toList());
        return users.isEmpty() ? new RegisteredUser() : users.get(0);
    }

    private RegisteredUser toRegisteredUser(Map<String, Object> result) {
        return new RegisteredUser(
                ((int) result.get("id")),
                ((String) result.get("name")),
                ((String) result.get("surname")),
                ((String) result.get("username")),
                ((String) result.get("password")),
                ((String) result.get("email"))
        );
    }

    public UserDto create(UserDto userDto) {
        String query = "INSERT INTO users (name, username, surname, password, email)" +
                " VALUES(:name, :username, :surname, :password, :email) ";

        try(Connection connection = sql2o.open()) {
            connection.createQuery(query)
                    .addParameter("name", userDto.name)
                    .addParameter("email", userDto.email)
                    .addParameter("surname", userDto.surname)
                    .addParameter("username", userDto.username)
                    .addParameter("password", userDto.password)
                    .executeUpdate();
        }
        return toUserDto(get(userDto));
    }

    private UserDto toUserDto(RegisteredUser registeredUser) {
        UserDto userDto = new UserDto();
        userDto.name = registeredUser.getName();
        userDto.surname = registeredUser.getSurname();
        userDto.email = registeredUser.getEmail();
        userDto.username = registeredUser.getUsername();
        return userDto;
    }
}
