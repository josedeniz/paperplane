package org.paperplane.repository;

import org.paperplane.domain.ChoiceDto;
import org.paperplane.domain.SearchDto;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import java.util.List;
import java.util.Map;

import static java.lang.Double.parseDouble;
import static java.lang.String.valueOf;
import static java.util.stream.Collectors.toList;

public class SearchRepository {

    private final Sql2o sql2o;

    public SearchRepository(String connectionUrl) {
        this.sql2o = sql2o(connectionUrl);
    }

    private Sql2o sql2o(String connectionUrl) {
        return new Sql2o(connectionUrl, "paperplane", "12345");
    }

    public List<ChoiceDto> execute(SearchDto searchDto) {
        String query = "SELECT S.Id, \"FROM\", \"TO\", PRICE, DEPARTURE_DATE, DEPARTURE_TIME, ARRIVAL_DATE_TIME, NAME As Company_Name " +
                       "FROM Stretch AS S " +
                       "INNER JOIN COMPANY AS C on C.ID = COMPANY_ID " +
                       "WHERE \"FROM\" = :origin " +
                       "AND \"TO\" = :destination " +
                       "AND TRIP_TYPE = :tripType";

        List<Map<String, Object>> queryResults;
        try(Connection connection = sql2o.open()) {
            queryResults = connection.createQuery(query)
                    .addParameter("origin", searchDto.origin)
                    .addParameter("destination", searchDto.destination)
                    .addParameter("tripType", searchDto.tripType)
                    .executeAndFetchTable()
                    .asList();
        }

        return queryResults.stream()
                .map(this::toChoiceDto)
                .collect(toList());
    }

    private ChoiceDto toChoiceDto(Map<String, Object> result) {
        ChoiceDto choiceDto = new ChoiceDto();
        choiceDto.id = valueOf((int) result.get("id"));
        choiceDto.origin = ((String) result.get("from"));
        choiceDto.price = parseDouble(((String) result.get("price")));
        choiceDto.destination = ((String) result.get("to"));
        choiceDto.companyName = ((String) result.get("company_name"));
        choiceDto.departureDate = result.get("departure_date") + " " + result.get("departure_time");
        choiceDto.arrivalDate = ((String) result.get("arrival_date_time"));
        return choiceDto;
    }
}
