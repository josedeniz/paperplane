package org.paperplane.controller.api;

import org.paperplane.actions.Register;
import org.paperplane.domain.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegisterController {

    private final Register register;

    @Autowired
    public RegisterController(Register register) {
        this.register = register;
    }

    @PostMapping("/api/register")
    public UserDto login(@RequestBody UserDto userDto) {
        return register.execute(userDto);
    }
}
