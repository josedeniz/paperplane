package org.paperplane.controller.api;

import org.paperplane.actions.Login;
import org.paperplane.actions.NotRegisteredUserException;
import org.paperplane.domain.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    private final Login login;

    @Autowired
    public LoginController(Login login) {
        this.login = login;
    }

    @PostMapping("/api/login")
    public UserDto login(@RequestBody UserDto userDto) throws NotRegisteredUserException {
        return login.execute(userDto);
    }
}
