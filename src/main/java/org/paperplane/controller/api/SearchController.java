package org.paperplane.controller.api;

import org.paperplane.actions.Search;
import org.paperplane.domain.ChoiceDto;
import org.paperplane.domain.SearchDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SearchController {

    private final Search search;

    @Autowired
    public SearchController(Search search) {
        this.search = search;
    }

    @GetMapping("/api/search")
    public List<ChoiceDto> search(@RequestParam("origin") String origin,
                            @RequestParam("tripType") String tripType,
                            @RequestParam("isSingle") boolean isSingle,
                            @RequestParam("returnDate") String returnDate,
                            @RequestParam("destination") String destination,
                            @RequestParam("departureDate") String departureDate,
                            @RequestParam("numberOfChildren") int numberOfChildren,
                            @RequestParam("numberOfPassengers") int numberOfPassengers) {
        final SearchDto searchDto = new SearchDto();
        searchDto.origin = origin;
        searchDto.tripType = tripType;
        searchDto.isSingle = isSingle;
        searchDto.returnDate = returnDate;
        searchDto.destination = destination;
        searchDto.departureDate = departureDate;
        searchDto.numberOfChildren = numberOfChildren;
        searchDto.numberOfPassengers = numberOfPassengers;
        return search.execute(searchDto);
    }

}
