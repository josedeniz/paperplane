package org.paperplane.controller.api;

import org.paperplane.actions.GetUserTrips;
import org.paperplane.actions.SaveTrip;
import org.paperplane.domain.ChoiceDto;
import org.paperplane.domain.UserTrip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserTripController {

    private final GetUserTrips getUserTrips;
    private final SaveTrip saveTrip;

    @Autowired
    public UserTripController(GetUserTrips getUserTrips, SaveTrip saveTrip) {
        this.saveTrip = saveTrip;
        this.getUserTrips = getUserTrips;
    }

    @GetMapping("/api/myTrips")
    public List<ChoiceDto> search(@RequestParam("userId") String userId) {
        return getUserTrips.execute(userId);
    }

    @PostMapping("/api/saveTrip")
    public UserTrip saveTrip(@RequestBody UserTrip userTrip) {
        saveTrip.execute(userTrip);
        return userTrip;
    }
}
