package org.paperplane.actions;

import org.paperplane.domain.SearchDto;
import org.paperplane.domain.TripDto;

import java.util.List;

public interface Action {
    List<TripDto> execute(SearchDto searchDto);
}
