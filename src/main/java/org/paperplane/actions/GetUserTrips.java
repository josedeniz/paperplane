package org.paperplane.actions;

import org.paperplane.domain.ChoiceDto;
import org.paperplane.repository.UserTripsRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetUserTrips {

    private final UserTripsRepository userTripsRepository;

    public GetUserTrips(UserTripsRepository userTripsRepository) {
        this.userTripsRepository = userTripsRepository;
    }

    public List<ChoiceDto> execute(String userId) {
        return userTripsRepository.getFor(userId);
    }
}
