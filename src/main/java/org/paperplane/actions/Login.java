package org.paperplane.actions;

import org.paperplane.domain.RegisteredUser;
import org.paperplane.domain.UserDto;
import org.paperplane.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Login {

    private final UserRepository userRepository;

    @Autowired
    public Login(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDto execute(UserDto userDto) throws NotRegisteredUserException {
        final RegisteredUser registeredUser = userRepository.get(userDto);
        if (registeredUser.getUsername().equals("")) throw new NotRegisteredUserException();
        return toUserDto(registeredUser);
    }

    private UserDto toUserDto(RegisteredUser registeredUser) {
        UserDto userDto = new UserDto();
        userDto.id = registeredUser.getId();
        userDto.name = registeredUser.getName();
        userDto.surname = registeredUser.getSurname();
        userDto.email = registeredUser.getEmail();
        userDto.username = registeredUser.getUsername();
        return userDto;
    }
}
