package org.paperplane.actions;

import org.paperplane.domain.ChoiceDto;
import org.paperplane.domain.SearchDto;
import org.paperplane.repository.SearchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class Search {

    private final SearchRepository searchRepository;

    @Autowired
    public Search(SearchRepository searchRepository) {
        this.searchRepository = searchRepository;
    }

    public List<ChoiceDto> execute(SearchDto searchDto) {
        return searchRepository.execute(searchDto)
                .stream()
                .map(c -> applyFilters(c, searchDto))
                .collect(toList());
    }

    private ChoiceDto applyFilters(ChoiceDto choiceDto, SearchDto searchDto) {
        choiceDto.price = calculatePrice(choiceDto, searchDto);
        choiceDto.scales = 0; // TODO
        return choiceDto;
    }

    private Double calculatePrice(ChoiceDto choiceDto, SearchDto searchDto) {
        final double adultPrice = choiceDto.price * searchDto.numberOfPassengers;
        final double childrenPrice = (choiceDto.price / 2) * searchDto.numberOfChildren;
        return adultPrice + childrenPrice;
    }
}
