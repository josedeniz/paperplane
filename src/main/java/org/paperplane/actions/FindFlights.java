package org.paperplane.actions;

import org.paperplane.PaperplaneDateFormatter;
import org.paperplane.domain.SearchDto;
import org.paperplane.domain.Trip;
import org.paperplane.domain.TripDto;
import org.paperplane.repository.TripRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class FindFlights implements Action {
    private final TripRepository tripRepository;

    @Autowired
    public FindFlights(TripRepository tripRepository) {
        this.tripRepository = tripRepository;
    }

    @Override
    public List<TripDto> execute(SearchDto searchDto) {
        return tripRepository.getTrips(searchDto)
                .stream()
                .map(t -> applyParameters(t, searchDto))
                .collect(toList());
    }

    private TripDto applyParameters(Trip flight, SearchDto searchDto) {
        final TripDto dto = new TripDto();
        /*dto.origin = flight.origin();
        dto.destination = flight.destination();
        dto.price = applyPrice(flight, searchDto);
        dto.arrivalDateTime = parseDate(flight.arrivalTime());
        dto.departureDateTime = parseDate(flight.departureTime());*/
        dto.numberOfChildren = searchDto.numberOfChildren;
        dto.numberOfPassengers = searchDto.numberOfPassengers;
        return dto;
    }

    private String parseDate(LocalDateTime date) {
        return PaperplaneDateFormatter.parseDateTimeToString(date);
    }

    private Double applyPrice(Trip trip, SearchDto searchDto) {
        return applyAdultPrice(trip, searchDto) + applyChildrenPrice(trip, searchDto);
    }

    private Double applyAdultPrice(Trip trip, SearchDto dto) {
        return trip.price() * dto.numberOfPassengers;
    }

    private Double applyChildrenPrice(Trip trip, SearchDto dto) {
        return (trip.price() / 2) * dto.numberOfChildren;
    }

}
