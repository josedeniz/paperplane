package org.paperplane.actions;

import org.paperplane.domain.UserDto;
import org.paperplane.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Register {

    private final UserRepository userRepository;

    @Autowired
    public Register(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDto execute(UserDto userDto)  {
        return userRepository.create(userDto);
    }
}
