package org.paperplane.actions;

import org.paperplane.domain.UserTrip;
import org.paperplane.repository.UserTripsRepository;
import org.springframework.stereotype.Service;

@Service
public class SaveTrip {

    private final UserTripsRepository userTripsRepository;

    public SaveTrip(UserTripsRepository userTripsRepository) {
        this.userTripsRepository = userTripsRepository;
    }

    public void execute(UserTrip userTrip) {
        userTripsRepository.save(userTrip);
    }
}
