export const HOME_ROUTE = '/';
export const LOGIN_ROUTE = '/login';
export const PROFILE_ROUTE = '/profile';
export const REGISTER_ROUTE = '/register';
export const SEARCH_RESULT_ROUTE = '/search';