import React from 'react';
import {render} from 'react-dom';
import App from './App';
import {BrowserRouter} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.css';
import './styles/ionicons.min.css';
import './styles/owl.carousel.css';
import './styles/owl.theme.css';
import './styles/flexslider.css';
import './styles/main.css';

render(
    <BrowserRouter>
        <App />
    </BrowserRouter>,
    document.getElementById('app')
);
