import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {HOME_ROUTE, LOGIN_ROUTE, PROFILE_ROUTE} from '../routes';
import StorageWrapper from '../LocalStorageWrapper';

class NavBar extends Component {

    constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);
        this.showLogout = this.showLogout.bind(this);
        this.showNavBarLinks = this.showNavBarLinks.bind(this);
        this.showAvatarOrLogin = this.showAvatarOrLogin.bind(this);
        this.state = {
            user: {}
        };
    }

    componentDidMount() {
        const storageWrapper = StorageWrapper();
        const user = storageWrapper.getUser();
        this.setState({user})
    }

    showAvatarOrLogin() {
        if (!this.state.user.id) return <li><Link to={LOGIN_ROUTE}>Login</Link></li>;
        return (
            <li>
                <Link to={PROFILE_ROUTE}>
                    <span className="with-margin-right-4"><i className="ion-person"/></span>
                    <span className="with-margin-right-4">{this.state.user.name}</span>
                </Link>
            </li>
        );
    }

    showLogout() {
        if (this.state.user.id) return (
            <li className="with-margin-top-10 cursor-pointer" onClick={this.logout}>
                <span><i className="ion-log-out"/></span>
            </li>
        );
    }

    logout() {
        const storageWrapper = StorageWrapper();
        storageWrapper.resetUser();
        this.props.history.push(LOGIN_ROUTE);
    }

    showNavBarLinks() {
        if (this.props.onlyLogo) return;
        return (
            <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul className="nav navbar-nav navbar-right">
                    <li className="active"><Link to={HOME_ROUTE}>Home</Link></li>
                    {this.showAvatarOrLogin()}
                    {this.showLogout()}
                </ul>
            </div>
        );
    }

    render() {
        return (
            <nav className="navbar navbar-default navbar-fixed-top">
                <div className="container">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"/>
                            <span className="icon-bar"/>
                            <span className="icon-bar"/>
                        </button>
                        <Link className="navbar-brand" to={HOME_ROUTE}>
                            <i className="ion-paper-airplane"/> paper<span>plane</span>
                        </Link>
                    </div>

                    {this.showNavBarLinks()}
                </div>
            </nav>
        );
    }
}

export default NavBar;