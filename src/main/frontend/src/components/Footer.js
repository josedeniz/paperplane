import React from 'react';

const Footer = () => {
    return (
        <footer>
            <div className="container">
                <div className="row">
                    <div className="col-xs-4">
                        <div className="text-left">
                            &copy; Copyright PaperPlane
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    );
};

export default Footer;