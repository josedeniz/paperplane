import React, {Component} from 'react';
import applyCarousel from './applyCarousel';

import visit_1 from '../images/visit-1.png';
import visit_2 from '../images/visit-2.png';
import visit_3 from '../images/visit-3.png';

class TopPlaces extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        applyCarousel();
    }

    componentDidUpdate() {
        applyCarousel();
    }

    render() {
        return (
            <section className="visit section-wrapper">
                <div className="container">
                    <h2 className="section-title">
                        Top places to visit
                    </h2>
                    <p className="section-subtitle">
                        Beach? Snow? Paradise?
                    </p>

                    <div className="owl-carousel visit-carousel" id="">
                        <div className="item">
                            <img src={visit_1} alt="visit-image" className="img-responsive visit-item"/>
                        </div>
                        <div className="item">
                            <img src={visit_2} alt="visit-image" className="img-responsive visit-item"/>
                        </div>
                        <div className="item">
                            <img src={visit_3} alt="visit-image" className="img-responsive visit-item"/>
                        </div>
                        <div className="item">
                            <img src={visit_1} alt="visit-image" className="img-responsive visit-item"/>
                        </div>
                        <div className="item">
                            <img src={visit_2} alt="visit-image" className="img-responsive visit-item"/>
                        </div>
                        <div className="item">
                            <img src={visit_3} alt="visit-image" className="img-responsive visit-item"/>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default TopPlaces;
