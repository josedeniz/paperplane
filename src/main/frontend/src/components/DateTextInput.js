import React from 'react';

const DateTextInput = ({id, label, onChange, disabled = false}) => {
    return (
        <div className="col-md-3 col-sm-6">
            <label>{label}</label>
            <div className="input-group">
                <input id={id}
                       type="text"
                       disabled={disabled}
                       className="form-control border-radius border-right"
                       onBlur={onChange}
                       placeholder="dd/mm/yyyy"/>
                <span className="input-group-addon border-radius custom-addon">
                    <i className="ion-ios-calendar"/>
                </span>
            </div>
        </div>
    );
};

export default DateTextInput;