import React from 'react';
import NavBar from './NavBar';
import TextInput from './TextInput';
import {Link} from 'react-router-dom';
import RestClient from '../RestClient';
import SubmitButton from './SubmitButton';
import {HOME_ROUTE, REGISTER_ROUTE} from '../routes';
import StorageWrapper from '../LocalStorageWrapper';

const Login = ({history}) => {

    const state = {
        username: '',
        password: ''
    };

    function handleOnChangeUsername(event) {
        state.username = event.target.value;
    }

    function handleOnChangePassword(event) {
        state.password = event.target.value;
    }

    function send() {
        const storageWrapper = StorageWrapper();
        const restClient = RestClient();
        restClient.doPost('/api/login', {
            username: state.username,
            password: state.password
        })
        .then((user) => storageWrapper.setUser(user))
        .then(() => history.push(HOME_ROUTE));
    }

    return (
        <div>
            <NavBar onlyLogo={true}/>
            <div className="login-form">
                <TextInput id="username"
                           style="col-lg-12 col-md-12 col-sm-12"
                           onChange={handleOnChangeUsername}
                           placeholder="Username"/>
                <TextInput id="password"
                           type="password"
                           style="col-lg-12 col-md-12 col-sm-12"
                           onChange={handleOnChangePassword}
                           placeholder="Password"/>
                <SubmitButton text="Login"
                              onClick={send}
                              style="col-lg-12 col-md-12 col-sm-12"/>
                <Link className="with-padding-left-15" to={REGISTER_ROUTE}>No account. Create one!</Link>
            </div>
        </div>
    );
};

export default Login;