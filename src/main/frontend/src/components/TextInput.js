import React from 'react';

const TextInput = ({id, type = 'text', label, onChange, placeholder, style = 'col-md-3 col-sm-6'}) => {
    return (
        <div className={style}>
            <label>{label}</label>
            <div className="input-group">
                <input id={id}
                       type={type}
                       className="form-control border-radius border-right"
                       onBlur={onChange}
                       placeholder={placeholder}/>
                <span className="input-group-addon border-radius custom-addon"/>
            </div>
        </div>
    );
};

export default TextInput;