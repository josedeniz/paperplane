import React from 'react';

const SubmitButton = ({onClick, style = 'col-md-3 col-sm-6', text}) => {
    return (
        <div className={style}>
            <label className="text-white">Go</label>
            <div className="btn btn-default border-radius custom-button" onClick={onClick}>
                {text}
            </div>
        </div>
    );
};

export default SubmitButton;