import React from 'react';
import NavBar from './NavBar';
import Search from './Search';
import Offers from './Offers';
import TopPlaces from './TopPlaces';
import Footer from './Footer';

const Home = (props) => {
    return (
        <div>
            <NavBar history={props.history}/>
            <Search history={props.history}/>
            <Offers/>
            <TopPlaces/>
            <Footer/>
        </div>
    );
};

export default Home;