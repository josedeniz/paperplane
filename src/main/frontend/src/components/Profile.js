import moment from 'moment';
import NavBar from './NavBar';
import RestClient from '../RestClient';
import React, {Component} from 'react';
import StorageWrapper from '../LocalStorageWrapper';

class Profile extends Component {

    constructor(props) {
        super(props);
        this.mapResultsToRows = this.mapResultsToRows.bind(this);
        this.state = {
            results: []
        };
    }

    componentDidMount() {
        const storageWrapper = StorageWrapper();
        const url = `/api/myTrips?userId=${storageWrapper.getUser().id}`;
        const restClient = RestClient();
        restClient.doGet(url)
            .then(results => this.setState({results}));
    }

    mapResultsToRows() {
        const parseDate = (dateString) => {
            // yyyy-mm-dd hh:mm
            return moment(dateString, "YYYY-MM-DD HH:mm").format('LLL');
        };

        return this.state.results.map((r, i) => (
            <tr key={i}>
                <td>{r.tripType}</td>
                <td>{r.companyName}</td>
                <td>{r.origin} - {parseDate(r.departureDate)}</td>
                <td>{r.destination} - {parseDate(r.arrivalDate)}</td>
                <td>{r.price} &euro;</td>
                <td>{r.scales}</td>
            </tr>
        ));
    }

    render() {
        return (
            <div className="col-lg-10">
                <NavBar history={this.props.history}/>
                <h2 className="with-more-margin-top">My saved trips</h2>
                <table className="table _table-bordered">
                    <thead>
                    <tr>
                        <th>Trip type</th>
                        <th>Company Name</th>
                        <th>Departure</th>
                        <th>Arrival</th>
                        <th>Price</th>
                        <th>Scales</th>
                    </tr>
                    </thead>
                    <tbody>
                        {this.mapResultsToRows()}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Profile;