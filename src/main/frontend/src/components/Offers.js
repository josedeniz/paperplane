import React from 'react';

const Offers = () => {
    return (
        <section className="offer section-wrapper">
            <div className="container">
                <h2 className="section-title">
                    What we offer
                </h2>
                <p className="section-subtitle">
                    We are the best friend to travel with
                </p>
                <div className="row">
                    <div className="col-sm-3 col-xs-6">
                        <div className="offer-item">
                            <div className="icon">
                                <i className="ion-android-plane"/>
                            </div>
                            <h3>
                                Flights
                            </h3>
                            <p>
                                First or third class, we have offers for every of them
                            </p>
                        </div>
                    </div>

                    <div className="col-sm-3 col-xs-6">
                        <div className="offer-item">
                            <div className="icon">
                                <i className="ion-android-train"/>
                            </div>
                            <h3>
                                Trains
                            </h3>
                            <p>
                                Have you ever tried an interrail? You will repeat!
                            </p>
                        </div>
                    </div>

                    <div className="col-sm-3 col-xs-6">
                        <div className="offer-item">
                            <div className="icon">
                                <i className="ion-android-boat"/>
                            </div>
                            <h3>
                                Ferries
                            </h3>
                            <p>
                                Do you like travelling with your car? Take a look!
                            </p>
                        </div>
                    </div>

                    <div className="col-sm-3 col-xs-6">
                        <div className="offer-item">
                            <div className="icon">
                                <i className="ion-android-boat"/>
                            </div>
                            <h3>
                                Cruises
                            </h3>
                            <p>
                                Have you heard about "floating hotels"?
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Offers;