import React from 'react';
import NavBar from './NavBar';
import TextInput from './TextInput';
import {Link} from 'react-router-dom';
import RestClient from '../RestClient';
import SubmitButton from './SubmitButton';
import {HOME_ROUTE, LOGIN_ROUTE} from '../routes';
import StorageWrapper from '../LocalStorageWrapper';

const Register = ({history}) => {

    const state = {
        name: '',
        email: '',
        surname: '',
        username: '',
        password: ''
    };

    function handleOnChangeUsername(event) {
        state.username = event.target.value;
    }

    function handleOnChangePassword(event) {
        state.password = event.target.value;
    }

    function handleOnChangeName(event) {
        state.name = event.target.value;
    }

    function handleOnChangeSurname(event) {
        state.surname = event.target.value;
    }

    function handleOnChangeEmail(event) {
        state.email = event.target.value;
    }

    function send() {
        const restClient = RestClient();
        const storageWrapper = StorageWrapper();
        restClient.doPost('/api/register', {
            name: state.name,
            email: state.email,
            surname: state.surname,
            username: state.username,
            password: state.password
        }).then((user) => storageWrapper.setUser(user))
          .then(() => history.push(HOME_ROUTE));
    }

    return (
        <div>
            <NavBar onlyLogo={true}/>
            <div className="login-form">
                <TextInput id="username"
                           style="col-lg-12 col-md-12 col-sm-12"
                           onChange={handleOnChangeUsername}
                           placeholder="Username"/>
                <TextInput id="password"
                           type="password"
                           style="col-lg-12 col-md-12 col-sm-12"
                           onChange={handleOnChangePassword}
                           placeholder="Password"/>
                <TextInput id="name"
                           style="col-lg-12 col-md-12 col-sm-12"
                           onChange={handleOnChangeName}
                           placeholder="Name"/>
                <TextInput id="surname"
                           style="col-lg-12 col-md-12 col-sm-12"
                           onChange={handleOnChangeSurname}
                           placeholder="Surname"/>
                <TextInput id="email"
                           style="col-lg-12 col-md-12 col-sm-12"
                           onChange={handleOnChangeEmail}
                           placeholder="Email"/>
                <SubmitButton text="Register"
                              onClick={send}
                              style="col-lg-12 col-md-12 col-sm-12"/>
                <Link className="with-padding-left-15" to={LOGIN_ROUTE}>Have an account. Login</Link>
            </div>
        </div>
    );
};

export default Register;