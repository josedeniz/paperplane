import React, {Component} from 'react';
import TextInput from './TextInput';
import NumberTextInput from './NumberTextInput';
import DateTextInput from './DateTextInput';
import SubmitButton from './SubmitButton';

class Search extends Component {

    constructor(props) {
        super(props);
        this.search = this.search.bind(this);
        this.onOneWayChanged = this.onOneWayChanged.bind(this);
        this.onReturnChanged = this.onReturnChanged.bind(this);
        this.handleOnChangeOrigin = this.handleOnChangeOrigin.bind(this);
        this.handleOnChangeTripType = this.handleOnChangeTripType.bind(this);
        this.handleOnChangeChildren = this.handleOnChangeChildren.bind(this);
        this.handleOnChangeDepartDate = this.handleOnChangeDepartDate.bind(this);
        this.handleOnChangeReturnDate = this.handleOnChangeReturnDate.bind(this);
        this.handleOnChangePassengers = this.handleOnChangePassengers.bind(this);
        this.handleOnChangeDestination = this.handleOnChangeDestination.bind(this);
        this.state = {
            origin: 'LPA',
            isSingle: true,
            returnDate: '',
            destination: 'LGW',
            departureDate: '',
            tripType: 'flight',
            numberOfChildren: 0,
            numberOfPassengers: 1,
            placeHolders: {
                from: 'LPA',
                to: 'LGW',
                numberOfPassengers: 1,
                numberOfChildren: 0
            }
        };
    }

    search() {
        const url = `/search?origin=${this.state.origin}&destination=${this.state.destination}&departureDate=${this.state.departureDate}&returnDate=${this.state.returnDate}&numberOfPassengers=${this.state.numberOfPassengers}&numberOfChildren=${this.state.numberOfChildren}&isSingle=${this.state.isSingle}&tripType=${this.state.tripType}`;
        this.props.history.push(url);
    }

    handleOnChangeTripType(event) {
        const placeholders = {
            ferry: {
                from: 'Las Palmas',
                to: 'S/c de Tenerife',
            },
            train: {
                from: 'Madrid',
                to: 'Roma',
            },
            cruise: {
                from: 'Las Palmas',
                to: 'Agadir',
            },
            flight: {
                from: 'LPA',
                to: 'LGW',
            }
        };

        const tripType = event.target.value;
        const selected = placeholders[tripType];
        this.setState({
            tripType: tripType,
            origin: selected.from,
            destination: selected.to,
            placeHolders: Object.assign(this.state.placeHolders, selected)
        });
    }

    handleOnChangeOrigin(event) {
        this.setState({origin: event.target.value});
    }

    handleOnChangeDestination(event) {
        this.setState({destination: event.target.value});
    }

    handleOnChangeDepartDate(event) {
        this.setState({departureDate: event.target.value});
    }

    handleOnChangeReturnDate(event) {
        this.setState({returnDate: event.target.value});
    }

    handleOnChangePassengers(event) {
        this.setState({numberOfPassengers: event.target.value});
    }

    handleOnChangeChildren(event) {
        this.setState({numberOfChildren: event.target.value});
    }

    onOneWayChanged() {
        this.setState({isSingle: true});
    }

    onReturnChanged() {
        this.setState({isSingle: false});
    }

    render() {
        return (
            <section id="search" className="tour section-wrapper container">
                <h2 className="section-title">
                    Find a Trip
                </h2>
                <p className="section-subtitle">
                    Where would you like to go?
                </p>
                <div className="row">
                    <div className="col-md-6 col-sm-6 with-margin-bottom">
                        <input type="radio" name="oneWay"
                               checked={this.state.isSingle}
                               value="oneWay"
                               onChange={this.onOneWayChanged}/> One Way
                        <input type="radio" name="oneWay"
                               checked={!this.state.isSingle}
                               value="return"
                               className="with-margin-left"
                               onChange={this.onReturnChanged}/> Return <br/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-3 col-sm-6">
                        <div className="form-group">
                            <label htmlFor="tripType">Select a trip type</label>
                            <select className="form-control border-radius" id="tripType"
                                    onChange={this.handleOnChangeTripType}>
                                <option value="flight">Flight</option>
                                <option value="train">Train</option>
                                <option value="ferry">Ferry</option>
                                <option value="cruise">Cruise</option>
                            </select>
                        </div>
                    </div>
                    <TextInput id="from"
                               label="From"
                               onChange={this.handleOnChangeOrigin}
                               placeholder={this.state.placeHolders.from}/>
                    <TextInput id="to"
                               label="To"
                               onChange={this.handleOnChangeDestination}
                               placeholder={this.state.placeHolders.to}/>
                    <DateTextInput id="depart"
                                   label="Depart"
                                   onChange={this.handleOnChangeDepartDate}/>
                </div>
                <div className="row">
                    <DateTextInput id="return"
                                   label="Return"
                                   disabled={this.state.isSingle}
                                   onChange={this.handleOnChangeReturnDate}/>
                    <NumberTextInput id="passengers"
                                     min={this.state.placeHolders.numberOfPassengers}
                                     label="Number of passengers"
                                     icon="ion-person-stalker"
                                     onChange={this.handleOnChangePassengers}/>
                    <NumberTextInput id="children"
                                     min={this.state.placeHolders.numberOfChildren}
                                     label="Number of children"
                                     icon="ion-android-contacts"
                                     onChange={this.handleOnChangeChildren}/>
                    <SubmitButton text="Search" onClick={this.search}/>
                </div>
            </section>
        );
    }
}

export default Search;