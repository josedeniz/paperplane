import moment from 'moment';
import NavBar from './NavBar';
import React, {Component} from 'react';
import RestClient from '../RestClient';
import StorageWrapper from '../LocalStorageWrapper';

class SearchResults extends Component {

    constructor(props) {
        super(props);
        this.filter = this.filter.bind(this);
        this.saveTrip = this.saveTrip.bind(this);
        this.createFlight = this.createFlight.bind(this);
        this.filterByCompany = this.filterByCompany.bind(this);
        this.filterByNoScales = this.filterByNoScales.bind(this);
        this.addFiltersByCompany = this.addFiltersByCompany.bind(this);
        this.filterByCheapestFirst = this.filterByCheapestFirst.bind(this);
        this.state = {
            user: {},
            results: [
                /*this.createFlight('Ryanair', 30.0, 0),
                this.createFlight('Ryanair', 60.0, 0),
                this.createFlight('Norwegian', 80.0, 0),
                this.createFlight('Air Europa', 120.0, 1),
                this.createFlight('British Airlines', 140.0, 2),
                this.createFlight('Aer Lingus', 128.0, 0),*/
            ],
            filteredResults: [
                /*this.createFlight('Aer Lingus', 128.0, 0),
                this.createFlight('British Airlines', 140.0, 2),
                this.createFlight('Air Europa', 120.0, 1),
                this.createFlight('Norwegian', 80.0, 0),
                this.createFlight('Ryanair', 60.0, 0),
                this.createFlight('Ryanair', 30.0, 0)*/
            ],
            filters: {
                companies: {},
                noScales: false,
                cheapestFirst: false
            }
        };
    }

    createFlight(companyName, price, scales) {
        return {
            companyName: companyName,
            origin: 'LPA',
            departureDate: '2017-12-02 12:15',
            destination: 'EDI',
            price: price,
            arrivalDate: '2017-12-02 17:00',
            scales: scales
        }
    }

    componentDidMount() {
        const url = `/api/search${this.props.location.search}`;
        const restClient = RestClient();
        restClient.doGet(url)
            .then(results => this.setState({results, filteredResults: results}));
        const storageWrapper = StorageWrapper();
        const user = storageWrapper.getUser();
        this.setState({user});
    }

    saveTrip(trip) {
        const restClient = RestClient();
        restClient.doPost('/api/saveTrip', {
            userId: this.state.user.id,
            tripId: trip.id
        }).then(() => {
            const element = document.getElementById(`trip_${trip.id}`);
            element.classList.remove('ion-flag');
            element.classList.add('ion-checkmark-round');
        });
    }

    mapResultsToRows() {
        const parseDate = (dateString) => {
            // yyyy-mm-dd hh:mm
            return moment(dateString, "YYYY-MM-DD HH:mm").format('LLL');
        };

        const showSaveAction = (trip) => {
            if (this.state.user.id) return (
                <span className="text-blue cursor-pointer with-margin-right-4" onClick={() => this.saveTrip(trip)}>
                    <i id={`trip_${trip.id}`} className="ion-flag with-margin-right-4"/>
                </span>
            );
        };

        return this.state.filteredResults.map((r, i) => (
            <tr key={i}>
                <td>{r.companyName}</td>
                <td>{r.origin} - {parseDate(r.departureDate)}</td>
                <td>{r.destination} - {parseDate(r.arrivalDate)}</td>
                <td>{r.price} &euro;</td>
                <td>{r.scales}</td>
                <td>
                    {showSaveAction(r)}
                    <span className="text-blue">
                        <i className="ion-android-send"/>
                    </span>
                </td>
            </tr>
        ));
    }

    filterByCheapestFirst(event) {
        const checked = event.target.checked;
        this.setState(Object.assign(this.state.filters, {
            cheapestFirst: checked
        }));
        this.filter();
    }

    filterByNoScales(event) {
        const checked = event.target.checked;
        this.setState(Object.assign(this.state.filters, {
            noScales: checked
        }));
        this.filter();
    }

    filterByCompany(event) {
        const value = event.target.value;
        const checked = event.target.checked;
        this.setState(Object.assign(this.state.filters.companies, {
            [value]: checked
        }));
        this.filter();
    }

    filter() {
        const identity = true;
        const {cheapestFirst, noScales} = this.state.filters;
        const selectedCompanies = Object.entries(this.state.filters.companies)
            .filter(entry => entry[1])
            .map((entry => entry[0]));
        const filteredResults = [...this.state.results]
            .filter(r => noScales ? r.scales === 0 : identity)
            .filter(r => selectedCompanies.length > 0 ? selectedCompanies.includes(r.companyName) : identity);
        filteredResults.sort(r => cheapestFirst ? -r.price : r.price);
        this.setState({filteredResults});
    }

    addFiltersByCompany() {
        const companies = [...new Set(this.state.results.map(r => r.companyName))];
        return companies.map((c, i) => (
            <li>
                <input key={i}
                       type="checkbox"
                       value={c}
                       onChange={this.filterByCompany}/>
                <span key={`${i}_${i}`} className="with-margin-left">{c}</span>
            </li>
        ));
    }

    render() {
        return (
            <div className="with-margin-top">
                <div className="col-lg-2 with-more-margin-top no-padding-right">
                    <div className="with-border with-less-padding-top">
                        <label className="with-margin-left" htmlFor="filters">Filters</label>
                        <ul className="no-style-list less-padding-left" id="filters">
                            <li>
                                <input type="checkbox"
                                       onChange={this.filterByCheapestFirst}/>
                                <span className="with-margin-left">Cheapest first</span>
                            </li>
                            <li>
                                <input type="checkbox"
                                       onChange={this.filterByNoScales}/>
                                <span className="with-margin-left">No scales</span>
                            </li>
                        </ul>
                        <label className="with-margin-left" htmlFor="companies">Companies</label>
                        <ul className="no-style-list less-padding-left">
                            {this.addFiltersByCompany()}
                        </ul>
                    </div>
                </div>
                <div className="col-lg-10">
                    <NavBar history={this.props.history}/>
                    <table className="table _table-bordered">
                        <thead>
                        <tr>
                            <th>Company Name</th>
                            <th>Departure</th>
                            <th>Arrival</th>
                            <th>Price</th>
                            <th>Scales</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.mapResultsToRows()}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default SearchResults;