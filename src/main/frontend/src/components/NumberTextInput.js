import React from 'react';

const NumberTextInput = (props) => {
    return (
        <div className="col-md-3 col-sm-6">
            <label>{props.label}</label>
            <div className="input-group">
                <input id={props.id}
                       type="number"
                       min={props.min}
                       className="form-control border-radius border-right"
                       onBlur={props.onChange}
                       placeholder={props.min}/>
                <span className="input-group-addon border-radius custom-addon">
                    <i className={props.icon}/>
                </span>
            </div>
        </div>
    );
};

export default NumberTextInput;