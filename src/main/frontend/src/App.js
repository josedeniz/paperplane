import React, {Component} from 'react';
import Home from './components/Home';
import Login from './components/Login';
import Register from './components/Register';
import Profile from './components/Profile';
import {Route, Switch} from 'react-router-dom';
import SearchResults from './components/SearchResults';
import {HOME_ROUTE, LOGIN_ROUTE, PROFILE_ROUTE, REGISTER_ROUTE, SEARCH_RESULT_ROUTE} from './routes';

class App extends Component {
  render() {
    return (
        <Switch>
            <Route exact path={HOME_ROUTE} component={Home}/>
            <Route path={LOGIN_ROUTE} component={Login}/>
            <Route path={REGISTER_ROUTE} component={Register}/>
            <Route path={PROFILE_ROUTE} component={Profile}/>
            <Route path={SEARCH_RESULT_ROUTE} component={SearchResults}/>
        </Switch>
    );
  }
}

export default App;
