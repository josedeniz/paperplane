CREATE TABLE USERS
(
  ID       SERIAL PRIMARY KEY NOT NULL,
  NAME     TEXT               NULL,
  SURNAME  TEXT               NULL,
  USERNAME TEXT               NULL,
  EMAIL    TEXT               NULL,
  PASSWORD TEXT               NULL
);

CREATE TABLE CURRENCY
(
  ID     SERIAL PRIMARY KEY NOT NULL,
  NAME   TEXT               NULL,
  CODE   TEXT               NULL,
  SYMBOL TEXT               NULL
);

CREATE TABLE NOTIFICATIONS
(
  ID      SERIAL PRIMARY KEY   NOT NULL,
  USER_ID INT REFERENCES USERS NULL,
  SENT_AT DATE                 NULL,
  "FROM"  TEXT                 NULL,
  "TO"    TEXT                 NULL,
  SUBJECT TEXT                 NULL,
  BODY    TEXT                 NULL
);

CREATE TABLE COMPANY
(
  ID   SERIAL PRIMARY KEY NOT NULL,
  NAME TEXT               NULL,
  LOGO TEXT               NULL
);

CREATE TABLE TRIP
(
  ID                   SERIAL PRIMARY KEY      NOT NULL,
  IS_SINGLE            BOOLEAN                 NULL,
  NUMBER_OF_PASSENGERS INT                     NULL,
  NUMBER_OF_CHILDREN   INT                     NULL,
  PRICE                TEXT                    NULL,
  CURRENCY_ID          INT REFERENCES CURRENCY NULL,
  TRIP_TYPE            TEXT                    NULL
);

CREATE TABLE STRETCH
(
  ID                SERIAL PRIMARY KEY      NOT NULL,
  TRIP_TYPE         TEXT                    NULL,
  "FROM"            TEXT                    NULL,
  "TO"              TEXT                    NULL,
  PRICE             TEXT                    NULL,
  DEPARTURE_DATE    TEXT                    NULL,
  DEPARTURE_TIME    TEXT                    NULL,
  ARRIVAL_DATE_TIME TEXT                    NULL,
  COMPANY_ID        INT REFERENCES COMPANY  NULL
);

CREATE TABLE JOURNEY
(
  ID         SERIAL PRIMARY KEY        NOT NULL,
  TRIP_ID    INT REFERENCES TRIP       NULL,
  STRETCH_ID INT REFERENCES STRETCH    NULL
);

CREATE TABLE USER_TRIPS
(
  USER_ID INT REFERENCES USERS NOT NULL,
  TRIP_ID INT                  NOT NULL,
  PRIMARY KEY (USER_ID, TRIP_ID)
);

INSERT INTO USERS (NAME, SURNAME, USERNAME, EMAIL, PASSWORD)
VALUES
  ('foo', 'foo', 'foo', 'foo', 'foo'),
  ('bar', 'bar', 'bar', 'bar', 'bar');

INSERT INTO COMPANY (NAME, LOGO)
VALUES
  ('Ryanair', ''),
  ('Norwegian', ''),
  ('British Airways', ''),
  ('Easy Jet', ''),
  ('Jet2', ''),
  ('Fred Olsen', ''),
  ('Armas', ''),
  ('Euro Train', ''),
  ('Costa', ''),
  ('MSC', '');

INSERT INTO STRETCH (TRIP_TYPE, "FROM", "TO", PRICE, DEPARTURE_DATE, DEPARTURE_TIME, ARRIVAL_DATE_TIME, COMPANY_ID)
VALUES -- yyyy-MM-dd HH:mm
  ('flight', 'LPA', 'LGW', '40', '2017-12-01', '11:00', '2017-12-01 15:00', 1),
  ('flight', 'LPA', 'LGW', '95', '2017-12-01', '11:00', '2017-12-01 15:00', 2),
  ('flight', 'LPA', 'LGW', '141', '2017-12-01', '11:00', '2017-12-01 15:00', 3),
  ('flight', 'LPA', 'LGW', '148', '2017-12-01', '11:00', '2017-12-01 15:00', 4),
  ('flight', 'LPA', 'LGW', '155', '2017-12-01', '11:00', '2017-12-01 15:00', 5),
  ('flight', 'LGW', 'LPA', '80', '2017-12-01', '10:00', '2017-12-01 14:00', 1),
  ('flight', 'LGW', 'LPA', '110', '2017-12-01', '10:00', '2017-12-01 14:00', 2),
  ('flight', 'LGW', 'LPA', '125', '2017-12-01', '10:00', '2017-12-01 14:00', 3),
  ('flight', 'LGW', 'LPA', '168', '2017-12-01', '10:00', '2017-12-01 14:00', 4),
  ('flight', 'LGW', 'LPA', '225', '2017-12-01', '10:00', '2017-12-01 14:00', 5),
  ('ferry', 'Las Palmas', 'S/c de Tenerife', '40', '2017-12-01', '08:00', '2017-12-01 09:30', 6),
  ('ferry', 'Las Palmas', 'S/c de Tenerife', '20', '2017-12-01', '06:00', '2017-12-01 10:00', 7),
  ('ferry', 'S/c de Tenerife', 'Las Palmas', '40', '2017-12-01', '08:00', '2017-12-01 09:30', 6),
  ('ferry', 'S/c de Tenerife', 'Las Palmas', '20', '2017-12-01', '06:00', '2017-12-01 10:00', 7),
  ('train', 'Madrid', 'Roma', '345', '2017-12-01', '10:00', '2017-12-01 17:00', 8),
  ('cruise', 'Las Palmas', 'Agadir', '200', '2017-12-01', '10:00', '2017-12-01 14:00', 9),
  ('cruise', 'Las Palmas', 'Agadir', '225', '2017-12-01', '10:00', '2017-12-01 14:00', 10),
  ('cruise', 'Agadir', 'Las Palmas', '200', '2017-12-01', '10:00', '2017-12-01 14:00', 9),
  ('cruise', 'Agadir', 'Las Palmas', '225', '2017-12-01', '10:00', '2017-12-01 14:00', 10);

INSERT INTO USER_TRIPS (USER_ID, TRIP_ID)
VALUES
  (1, 1),
  (1, 2),
  (1, 6),
  (1, 8),
  (1, 9);